class Book
  # TODO: your code goes here!
  def title
    @title
  end

  def title=(title)
    @title = new_title(title)
  end

  private

  def new_title(title)
    no_cap = ["and", "in", "the", "of", "a", "an"]
    title.capitalize.split(" ").map {|word| no_cap.include?(word) ? word : word.capitalize }.join(" ")
  end


end
