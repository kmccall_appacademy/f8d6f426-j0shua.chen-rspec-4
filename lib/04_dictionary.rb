class Dictionary
  # TODO: your code goes here!

  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(dentry)
    if dentry.is_a? String
      dentry = {dentry => nil}
    end
    dentry.each {|k, v| @entries[k] = v}
  end

  def keywords
    @entries.keys.sort
  end

  def include?(kword)
    keywords.include?(kword)
  end

  def find(word)
    search_results = @entries.select {|k, v| k[0...word.length] == word}
  end

  def printable
    dict = []
    @entries.sort.each {|k, v| dict << "[#{k}] \"#{v}\""}
    dict.join("\n")
  end

end
