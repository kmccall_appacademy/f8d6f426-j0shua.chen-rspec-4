class Timer
  attr_accessor :seconds
  def initialize(seconds =0)
    @seconds = seconds
  end

  def time_string
    seconds = @seconds%60
    minutes = (@seconds/60) %60
    hours = @seconds/ 60**2
    "#{timez(hours)}:#{timez(minutes)}:#{timez(seconds)}"
  end

  def timez(x)
    return "0#{x}" if x <10
    return "#{x}" if x>=10
  end
end
